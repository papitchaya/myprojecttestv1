import { NgModule, Component } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ElementOfRiskComponent } from "./modules/element-of-risk/element-of-risk.component";
import { GPAComponent } from "./modules/gpa/gpa.component";
import { ProductComponent } from "./modules/product/product.component";
import { UserComponent } from "./modules/user/user.component";
import { UserFormComponent } from "./modules/user/user-form.component";

const routes: Routes = [
  { path: "elementofrisk", component: ElementOfRiskComponent },
  { path: "gpa", component: GPAComponent },
  { path: "product", component: ProductComponent },
  { path: "user", component: UserComponent },
  { path: "user/add", component: UserFormComponent },
  { path: "user/:id/edit", component: UserFormComponent }
  // {
  //   path: 'user' , component: UserComponent ,
  //   children :[
  //        {path: 'add' , component: UserFormComponent}
  //       //  {path: 'edit/:id' , component: UserComponent}
  //       ]
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
