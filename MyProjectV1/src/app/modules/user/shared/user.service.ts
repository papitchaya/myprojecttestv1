import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { ApiModel } from "src/app/model/api.model";
import { Users } from "./user.model";
import { Observable } from "rxjs";

@Injectable()
export class UserService {
  apiURL: string = "https://reqres.in/api";
  constructor(private httpClient: HttpClient, private route: Router) {}

  // getUsersAll()
  // {
  //      return this.httpClient.get(this.apiURL + '/users?page=2');
  // }

  getUsersAll() {
    const url = `${this.apiURL}/users`;
    return this.httpClient.get<ApiModel>(url);
  }

  getUserByID(_id: number) {
    const url = `${this.apiURL}/users/${_id}`;
    return this.httpClient.get<ApiModel>(url);
  }

  updateUserById(users: Users) {
    const url = `${this.apiURL}/users/${users.id}`;
    return this.httpClient.put(url, users);
  }

  addNewUser(users: Users) {
    const url = `${this.apiURL}/users`;
    return this.httpClient.post(url, users);
  }
  DeleteUserByID(_id: number) {
    const url = `${this.apiURL}/users/${_id}`;
    return this.httpClient.delete<ApiModel>(url);
  }
}
