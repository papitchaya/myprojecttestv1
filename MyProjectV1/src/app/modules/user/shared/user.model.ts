export class Users {
  public id: number;
  public firstname: string;
  public lastname: string;
  public email: string;
  public IsThai: boolean;
  public status: string;
}
