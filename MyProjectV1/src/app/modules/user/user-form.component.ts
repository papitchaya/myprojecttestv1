import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { UserService } from "./shared/user.service";
import { Users } from "./shared/user.model";

@Component({
  selector: "app-user-form",
  templateUrl: "./user-form.component.html"
})
export class UserFormComponent implements OnInit {
  userForm: FormGroup;
  editMode: boolean = false;
  id: number;
  userList: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _userService: UserService
  ) {}

  ngOnInit(): void {
    this.id = this._activatedRoute.snapshot.params["id"];
    if (this.id) {
      this.editMode = true;
    } else {
      this.editMode = false;
    }
    this.userForm = new FormGroup({
      firstname: new FormControl("", [Validators.required]),
      lastname: new FormControl("", [Validators.required]),
      email: new FormControl("", [Validators.required]),
      chkIsThai: new FormControl(false),
      status: new FormControl("S")
    });

    if (this.editMode) {
      this.getUserById(this.id);
    }
  }

  getUserById(_id: number) {
    this._userService.getUserByID(_id).subscribe(res => {
      this.userList = res.data;
      this.userForm.patchValue({
        firstname: this.userList.first_name,
        lastname: this.userList.last_name,
        email: this.userList.email
      });
    });
  }
  onSubmit() {
    console.log("onSubmit");
    let formValue = this.userForm.value;
    let users = new Users();
    users.firstname = formValue.first_name;
    users.lastname = formValue.last_name;
    users.email = formValue.email;
    users.IsThai = formValue.chkIsThai;
    users.status = formValue.status;

    if (this.editMode) {
      //update : put
      users.id = this.id;
      console.log("update");
      this._userService.updateUserById(users).subscribe(
        res => {
          console.log("DataUsers", users);
          console.log("res", res);
          console.log("Update Success");
          this._router.navigate(["../../"], {
            relativeTo: this._activatedRoute
          });
        },
        error => console.log("Update Error")
      );
    } else {
      //insert : post
      //users.id =max userid;
      console.log("insert");
      this._userService.addNewUser(users).subscribe(
        res => {
          console.log("res", res);
          console.log("insert Success");
          this._router.navigate(["../"], { relativeTo: this._activatedRoute });
        },
        error => console.log("insert Error")
      );
    }
  }

  onCancel() {
    console.log("button is clicked!");
    let path = this.editMode ? "../../" : "../";
    this._router.navigate([path], { relativeTo: this._activatedRoute });
  }
}
