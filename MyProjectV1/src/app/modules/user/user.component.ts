import { Component, OnInit } from "@angular/core";
import { UserService } from "./shared/user.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html"
})
export class UserComponent implements OnInit {
  userList: any[] = [];

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.getUsersAll();
  }
  getUsersAll() {
    this.userService.getUsersAll().subscribe(res => {
      this.userList = res.data;
      console.log("this.dataList", this.userList);
    });
  }

  onDelete(_id: number) {
    if (confirm("ต้องการลบข้อมูลใช่หรือไม่")) {
      this.userService.DeleteUserByID(_id).subscribe(
        () => {
          console.log("onDelete Success");
          this.getUsersAll();
        },
        error => console.log("onDelete Error")
      );
    }
  }
}
