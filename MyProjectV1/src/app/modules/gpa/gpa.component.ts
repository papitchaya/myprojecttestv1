import { Component, OnInit } from '@angular/core';
import { GPAService } from './shared/gpa.service';

@Component({
    selector: 'app-gpa',
    templateUrl: './gpa.component.html'
})
export class GPAComponent implements OnInit {
    gpaList: any;
    constructor(
        private gpaService: GPAService
    ) { }

    ngOnInit(): void {
        this.gpaService.getGPA()
        .subscribe(data => this.gpaList = data);
     }
}
