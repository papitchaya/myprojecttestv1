import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';


@Injectable()
export class GPAService {
    apiURL: string ='https://api.reg.cmu.ac.th/api/student/540741052/gpa';
    constructor(
        private httpClient: HttpClient ,
        private route: Router
    ){}

    getGPA()
    {
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'text/plain',
              'Authorization': 'Basic ' + btoa('IT3:KM92LASaxAI')
            })
          };

        return this.httpClient.get(this.apiURL, httpOptions);

    }


}