import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ElementOfRisk } from './element-of-risk.model';


@Injectable()
export class ElementOfRiskService {
    apiURL:string = 'http://api.mis.cmu.ac.th/risk/v1.0/api';
    token:string = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJFbXBsb3llZUlETUlTIjoiMDE4NTIwIiwiZW1haWxfY211IjoicGFwaXRjaGF5YS53QGNtdS5hYy50aCJ9.L9RnXP_y5WKFda3qfAPgHFDc7a2OBDlgCgnEGa5zf54';
    constructor(
        private httpClient: HttpClient ,
        private route: Router
    ){}

    private getToken() {
        return new HttpHeaders({Authorization: 'Bearer ' + this.token});
    }

    getElementOfRiskAll()
    {
         return this.httpClient.get<ElementOfRisk[]>(`${this.apiURL}/elementOfRisk/s`, { headers: this.getToken()});
    }

}