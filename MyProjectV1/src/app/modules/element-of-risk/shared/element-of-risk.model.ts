export class ElementOfRisk {
    public StrategicTypeID: number;
    public StrategicTypeName: string;
    public Description: string;
    public OrderSort: number;
    public StatusUse: number;
    public Status: string;
}