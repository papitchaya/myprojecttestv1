import { Component, OnInit } from '@angular/core';
import { ElementOfRiskService } from './shared/element-of-risk.service';

@Component({
    selector: 'app-element-of-risk',
    templateUrl: './element-of-risk.component.html'
})
export class ElementOfRiskComponent implements OnInit {
    ElementList: any;
    constructor(
        private elementOfRiskService: ElementOfRiskService
    ) { }

    ngOnInit(): void {
        this.elementOfRiskService.getElementOfRiskAll()
        .subscribe(data => this.ElementList = data);
     }
}
