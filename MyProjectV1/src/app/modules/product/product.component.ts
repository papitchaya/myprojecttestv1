import { Component, OnInit } from '@angular/core';
import { ProductService } from './shared/product.service';

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html'
})
export class ProductComponent implements OnInit {
    dataList: any;
    constructor(
        private productService: ProductService
    ) {}

    ngOnInit(): void {
        this.productService.getProductAll()
        .subscribe(
            data =>
            {this.dataList = data ;
            console.log('this.dataList',this.dataList) ;
        });
        // this.productService.getDataAll()
        // .subscribe(
        //     data =>
        //     {this.dataList = data ;
        //     console.log('this.dataList',this.dataList) ;
        // });
    }
}
