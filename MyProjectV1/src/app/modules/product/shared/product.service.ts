import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ProductService {
    apiURL: string ='https://jsonplaceholder.typicode.com/posts';
    constructor(
        private httpClient: HttpClient ,
        private route: Router
    ){}

    getDataAll()
    {
         return this.httpClient.get(this.apiURL);
    }

    getProductAll()
    {
         return this.httpClient.get('https://localhost:44335/api/Product');
    }
}