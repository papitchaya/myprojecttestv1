import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { MenuComponent } from "./layout/menu/menu.component";
import { ElementOfRiskComponent } from "./modules/element-of-risk/element-of-risk.component";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { GPAComponent } from "./modules/gpa/gpa.component";
import { ElementOfRiskService } from "./modules/element-of-risk/shared/element-of-risk.service";
import { GPAService } from "./modules/gpa/shared/gpa.service";
import { ProductComponent } from "./modules/product/product.component";
import { ProductService } from "./modules/product/shared/product.service";
import { UserComponent } from "./modules/user/user.component";
import { UserService } from "./modules/user/shared/user.service";
import { UserFormComponent } from "./modules/user/user-form.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ElementOfRiskComponent,
    GPAComponent,
    ProductComponent,
    UserComponent,
    UserFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ElementOfRiskService, GPAService, ProductService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule {}
